<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<!-- start page title -->
	    <title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
<!-- end page title -->
	    <meta name="viewport" content="width=700" />
		<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<?php wp_head(); ?>
	</head>
	<body>
		<div id="view">
			<div id="nav">
<!-- start navbar -->
				<!--PageHeaderFmt-->
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
<!-- end navbar -->
      		</div>
			<!--PageTitleFmt-->
      		<div id="headline">
<!-- start page name -->
                <h1><a href="<?php echo get_settings('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
<!-- end page name -->
			</div>
<!-- <div class="description"><?php bloginfo('description'); ?></div> -->