<?php get_header(); ?>


	<div id="content" >
<?php get_sidebar(); ?>

		<?php if (have_posts()) : ?>

		 <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
<?php /* If this is a category archive */ if (is_category()) { ?>
		<h2 class="pagetitle">Archive for the '<?php echo single_cat_title(); ?>' Category</h2>

 	  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h2 class="pagetitle">Archive for <?php the_time('j F Y'); ?></h2>

	 <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h2 class="pagetitle">Archive for <?php the_time('F Y'); ?></h2>

		<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h2 class="pagetitle">Archive for <?php the_time('Y'); ?></h2>

	  <?php /* If this is a search */ } elseif (is_search()) { ?>
		<h2 class="pagetitle">Search Results</h2>

	  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h2 class="pagetitle">Author Archive</h2>

		<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h2 class="pagetitle">Blog Archives</h2>

		<?php } ?>


<! Show navigation buttons only if Javascript, a requirement of infinite scroll, is disabled or infinite scroll itself is deactivated-->
<?php
//Get Jetpack enabled modules, this is an array
$jetpack_options = get_option( 'jetpack_active_modules' );
//If infinite scroll isn't enabled: We're checking the array of enabled Jetpack modules to see if 'infinite-scroll' is a value, if it isn't...
if (in_array('infinite-scroll', $jetpack_options) == false) {
	//Add the navigation div
	?>
		<p><br /><br /><?php next_posts_link('&laquo; Previous Entries ...') ?><?php previous_posts_link('... Next Entries &raquo;') ?></p>
	<?php
//Otherwise if infinite scroll is enabled...
} else {
	//Do we have Javascript enabled? If no add the navigation div
	?>
	<noscript>
		<p><br /><br /><?php next_posts_link('&laquo; Previous Entries ...') ?><?php previous_posts_link('... Next Entries &raquo;') ?></p>
	</noscript>
<?php
}
?>

<! Load the posts-->		
<?php get_template_part( 'post-loop' ); ?>

<! Show navigation buttons only if Javascript, a requirement of infinite scroll, is disabled or infinite scroll itself is deactivated-->
<?php
//Get Jetpack enabled modules, this is an array
$jetpack_options = get_option( 'jetpack_active_modules' );
//If infinite scroll isn't enabled: We're checking the array of enabled Jetpack modules to see if 'infinite-scroll' is a value, if it isn't...
if (in_array('infinite-scroll', $jetpack_options) == false) {
	//Add the navigation div
	?>
		<p><br /><br /><?php next_posts_link('&laquo; Previous Entries ...') ?><?php previous_posts_link('... Next Entries &raquo;') ?></p>
	<?php
//Otherwise if infinite scroll is enabled...
} else {
	//Do we have Javascript enabled? If no add the navigation div
	?>
	<noscript>
		<p><br /><br /><?php next_posts_link('&laquo; Previous Entries ...') ?><?php previous_posts_link('... Next Entries &raquo;') ?></p>
	</noscript>
<?php
}
?>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<?php include (TEMPLATEPATH . '/searchform.php'); ?>

	<?php endif; ?>

	</div>

<?php get_footer(); ?>