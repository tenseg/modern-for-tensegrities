<?php while (have_posts()) : the_post(); ?>

			<div class="post" id="post-<?php the_ID(); ?>">
				<h5>
				<?php the_time('j F') ?> . <?php comments_popup_link('Comment', '1 Comment', '% Comments'); ?><?php edit_post_link('Edit', ' . ', ''); ?></h5>
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h2>

				<div class="entry">
					<?php the_content("Continue reading ".the_title('', '', false)." &raquo;"); ?>
				</div>

			</div>

		<?php endwhile; ?>