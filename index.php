<?php get_header(); ?>

			<div id="content">
<!-- start of content -->
<?php get_sidebar(); ?>

	<?php if (have_posts()) : ?>
		<! Load the posts-->
		<?php get_template_part( 'post-loop' ); ?>

<! Show navigation buttons only if Javascript, a requirement of infinite scroll, is disabled or infinite scroll itself is deactivated-->
<?php
//Get Jetpack enabled modules, this is an array
$jetpack_options = get_option( 'jetpack_active_modules' );
//If infinite scroll isn't enabled: We're checking the array of enabled Jetpack modules to see if 'infinite-scroll' is a value, if it isn't...
if (in_array('infinite-scroll', $jetpack_options) == false) {
	//Add the navigation div
	?>
		<p><br /><br /><?php next_posts_link('&laquo; Previous Entries ...') ?><?php previous_posts_link('... Next Entries &raquo;') ?></p>
	<?php
//Otherwise if infinite scroll is enabled...
} else {
	//Do we have Javascript enabled? If no add the navigation div
	?>
	<noscript>
		<p><br /><br /><?php next_posts_link('&laquo; Previous Entries ...') ?><?php previous_posts_link('... Next Entries &raquo;') ?></p>
	</noscript>
<?php
}
?>


	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php include (TEMPLATEPATH . "/searchform.php"); ?>

	<?php endif; ?>

<!-- end of content -->
			</div>

<?php get_footer(); ?>
