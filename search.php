<?php get_header(); ?>

	<div id="content" class="narrowcolumn">

<?php get_sidebar(); ?>

	<?php if (have_posts()) : ?>

		<h2 class="pagetitle">Search Results</h2>

			<p><?php next_posts_link('&laquo; Previous Entries ...') ?><?php previous_posts_link('... Next Entries &raquo;') ?></p>

				<?php get_template_part( 'post-loop' ); ?>


			<p><br /><br /><?php next_posts_link('&laquo; Previous Entries ...') ?><?php previous_posts_link('... Next Entries &raquo;') ?></p>

	<?php else : ?>

		<h2 class="center">No posts found. Try a different search?</h2>
		<?php include (TEMPLATEPATH . '/searchform.php'); ?>

	<?php endif; ?>

	</div>


<?php get_footer(); ?>