# Modern for Tensegrities Theme

- Contributors: Alexander Celeste and Eric Celeste
- Tags: iweb
- Requires at least: 3.5
- Tested up to: 4.1.1
- Stable tag: 1.0.1
- License: The MIT License 
- License URI: http://opensource.org/licenses/MIT

This is a clean theme based on iWeb's Kubrick theme.

## Description

A theme designed to match the Kubrick theme in iWeb. Regular site visitors will barely see a difference between iWeb generated pages and wp.

## Features

- Full support for Jetpack features like infinite scroll and comments
- Widgetized sidebar to the right of site content
- Customizable footer credits

## Installation

The easiest way to install and maintain this theme is with [GitHub Updater](https://github.com/afragen/github-updater). The great thing about using GitHub Updater is that the theme will then be able to be updated through the regular WordPress update process.

Of course, if you wish to install and update the theme manually, you are welcome to do so.

### Installing with GitHub Updater
1. First make sure you have [GitHub Updater](https://github.com/afragen/github-updater) itself installed.
2. In the WordPress Dashboard go to *Settings > GitHub Updater > Install Theme*.
3. Enter `tenseg/modern-for-tensegrities` as the Theme URI and make sure the Remote Repository Host is `Bitbucket`. Then click the "Install Theme".
4. Activate the Modern for Tensegrities theme after it has installed.

When the theme is updated, GitHub Updater will make sure that WordPress knows this and presents the usual offers to update the theme for you.

### Installing manually
1. Download the [master.zip](https://bitbucket.org/tenseg/modern-for-tensegrities/get/master.zip) file from the Modern for Tensegrities repository.
2. This file will have an odd name. Rename it to `modern-for-tensegrities.zip`.
3. In the WordPress Dashboard go to *Themes > Add New* and click on the small "this page" link offering to upload a plugin in .zip format.
4. Use the "Choose File" button and choose the "modern-for-tensegrities.zip" file you just renamed.
5. Activate the Modern for Tensegrities theme after it has installed.

When the theme is updated, you will have to go through this same process to download and install the new version.

## Notes

This theme is really designed only for use by family members who still use the legacy iWeb product. It is not completely designed for wider public use.