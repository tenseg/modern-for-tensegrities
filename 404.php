<?php get_header(); ?>

	<div id="content">

<?php get_sidebar(); ?>

<h2>Take a deep breath. This is a 404 page.</h2>

<p>We couldn't find what you are looking for.</p>

<p>Do not pass <strong>go</strong>; do not collect $200.</p>

<p>Instead, try one of the following:</p>

<ul>
	<li>Hit the "back" button on your browser.</li>

	<li>Head on over to the <a href="<?php echo get_settings('home'); ?>/">front page</a>.</li>

	<li>Try searching using the form in the sidebar.</li>

	<li>Use the archives in the sidebar.</li>

	<li>Punt.</li>
</ul>

<p>Blessings!</p>

	</div>

<?php get_footer(); ?>