## Changelog

####1.0.1
* sidebar is now widgetized area "Sidebar"
* footer credits are now customizable in theme options

####1.0
* theme now managed by Bitbucket repository and updated via GitHub Updater plugin

####0.2
* support for jetpack

####0.1
* initial release