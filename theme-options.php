<?php
// found at http://themeshaper.com/sample-theme-options/

add_action( 'admin_menu', 'modern_theme_options' );

/**
 * Load up the menu page
 */
function modern_theme_options() {
	add_theme_page( __( 'Modern Options' ), __( 'Modern Options' ), 'edit_theme_options', 'modern_theme_options', 'modern_handler' );
}

/**
 * Create the options page
 */
function modern_handler() {

	if (!current_user_can('edit_theme_options'))  {
		wp_die( __('You do not have sufficient permissions to access this page.') );
	}

	$options = get_option( 'modern_options' );

	if(!$options){ // if the customizations don't exist yet
		$defaults = array(); // create an empty array
		$defaults['footercredits'] = ""; // set the default infinite scroll footer credits
 
		add_option( "modern_options", $defaults ); // add our customizations to the database
	}

	if(isset($_POST['submit'])){ // If they've clicked the submit button
		$options['footercredits'] = htmlentities(stripslashes($_POST['modernfootercredits'])); // save the new footer credits
		update_option('modern_options',$options); // update the database
 
		$url = get_bloginfo('url'); // get the blog url from the database
		$updated_html = "
			<div id='message' class='updated below-h2'>
				<p>Theme updated. <a href='{$url}'>Visit your site</a> to see how it looks.</p>
			</div>
		"; // some HTML to show that the options were updated
    }

?>
		<div class="wrap">
			<div id="icon-themes" class="icon32"><br /></div>
			<h2>Modern Theme Options</h2>
			<?php echo $updated_html; ?>
 
			<form action="" method="post" id="modern-form">
				<table class="form-table">
					<tr>
						<td colspan="2">
							<h3>Footer Credits</h3><p>The Modern theme allows you to set the text used as the footer credits.</p>
						</td>
					</tr>
					<tr>
						<th scope="row" valign="top"><label for="sidebarurl">Footer credits: </label></th>
						<td>
							<input type="text" name="modernfootercredits" value="<?php echo modernoption('footercredits') ?>" size="50">
						</td>
					</tr>
                </table>
 
				<br /><span class='submit' style='border: 0;'><input type='submit' name='submit' value='Save Options' /></span>
			</form>
		</div>
<?php
}

if(get_option('modern_options')){ // if the customizations exist
    function modern_header_addition(){ // display function for the customizations
        $options = get_option('modern_options'); // get our customizations from the database

    }
    add_action( 'wp_head', 'modern_header_addition' ); // attach our display function to the wp_head function
}